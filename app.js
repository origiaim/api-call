const express = require("express");
const process = require('./index');
const fs = require("fs");
var http = require('http');
const app = express();

app.use(function (req, res, next) {
    // allow 2 port access to each other
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:5000');

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

app.get("/get", async function (req, res) {
    const getData = await process.getValue();
    res.json(getData);
});

app.get("/set", async function (req, res) {
    const getData = await process.addValue(req.query.name, req.query.email);
    res.json(getData);
});

app.get("/update", async function (req, res) {
    const getData = await process.updateValue(req.query.name, req.query.email);
    res.json(getData);
});


fs.readFile('./home.html', function (err, html) {
    if (err) throw err;
    http.createServer(function (request, response) {
        response.writeHeader(200, { "Content-Type": "text/html" });
        response.write(html);
        response.end();
    }).listen('5000');
});