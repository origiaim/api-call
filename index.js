const fs = require("fs");
const jsonFile = require('./data.json');

const addValue = async (userName, email) => {
    const data = jsonFile;
    const dataExist = await findElement(data, "email", email);
    if (dataExist != undefined) {
        return { status: 409, message: "Data already exist" };
    }
    else {
        const feed = {
            "name": userName,
            "email": email
        }
        data.push(feed);
        try {
            fs.writeFile("data.json", JSON.stringify(data), 'utf8', function (err) {
                if (err) throw err;
            });
            return { status: 200, message: "Value added successfully", res: true };
        } catch (error) {
            return { status: 500, message: "Unexpected Server error", err: error };
        }
    }
}

const getValue = async () => {
    try {
        const allValues = jsonFile;
        return { status: 200, message: "Display result", res: allValues };
    } catch (error) {
        return { status: 500, message: "Unexpected Server error", err: error };
    }
}

const updateValue = async (name, email) => {
    const allValues = jsonFile;
    try {
        const result = await findElement(allValues, "email", email)
        if (result != undefined) {
            allValues.pop(result);
            result.name = name;
            allValues.push(result)
            fs.writeFile("data.json", JSON.stringify(allValues), 'utf8', function (err) {
                if (err) throw err;
            });
            return { status: 200, message: "Value update successfully", res: true };
        }
        else {
            return { status: 409, message: "email ID not found" };
        }
    } catch (error) {
        return { status: 500, message: "Unexpected Server error", err: error };
    }
}

async function findElement(arr, propName, propValue) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][propName] == propValue) {
            return arr[i];
        }
    }
}
module.exports = { updateValue, getValue, addValue };